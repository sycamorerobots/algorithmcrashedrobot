/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.algorithms;

import java.util.Vector;
import it.diunipi.volpi.sycamore.engine.Observation;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.engine.SycamoreObservedRobot;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.algorithms.AlgorithmImpl;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 * @author harry
 *
 */
@PluginImplementation
public class CrashedRobot extends AlgorithmImpl<Point2D> {

	private String	_author				= "Harish Prakash";
	private String	_shortDescription	= "A Crashed Robot";
	private String	_description		= "A Crashed Robot";
	private String	_pluginName			= "CrashedRobot";
	private TYPE	_type				= TYPE.TYPE_2D;

	@Override
	public void init(SycamoreObservedRobot<Point2D> robot) {}

	@Override
	public Point2D compute(Vector<Observation<Point2D>> observations, SycamoreObservedRobot<Point2D> caller) {

		setFinished(true);
		return null;
	}

	@Override
	public String getReferences() {

		return null;
	}

	@Override
	public TYPE getType() {

		return _type;
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return CrashedRobotConfigurationPanel.getUniqueInstance();
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}

}
