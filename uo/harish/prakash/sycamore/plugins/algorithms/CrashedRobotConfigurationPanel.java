/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.algorithms;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import javax.swing.JButton;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine;
import it.diunipi.volpi.sycamore.engine.SycamoreRobot;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;

/**
 * @author harry
 *
 */
public class CrashedRobotConfigurationPanel extends SycamorePanel {

	private JButton				buttonPrint;
	private SycamoreEngine<?>	engine;

	private static CrashedRobotConfigurationPanel uniqueInstance;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CrashedRobotConfigurationPanel() {

		setupGUI();
	}

	private void setupGUI() {

		GridBagLayout layout = new GridBagLayout();
		layout.rowWeights = new double[] { 1 };
		layout.columnWeights = new double[] { Double.MIN_VALUE, 1 };
		setLayout(layout);

		GridBagConstraints c;
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		add(getButtonPrinit(), c);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setAppEngine(SycamoreEngine appEngine) {

		this.engine = appEngine;
	}

	@Override
	public void updateGui() {

		// TODO Auto-generated method stub

	}

	@Override
	public void reset() {

		// TODO Auto-generated method stub

	}

	/**
	 * @return the buttonPrinit
	 */
	private JButton getButtonPrinit() {

		if (buttonPrint == null) {
			buttonPrint = new JButton("Print RobotPositions");
			buttonPrint.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					if (getEngine() == null) {
						return;
					}
					System.out.println("### Printing Positions ###");
					Iterator<?> robots = getEngine().getRobots().iterator();
					while (robots.hasNext()) {
						SycamoreRobot<?> robot = (SycamoreRobot<?>) robots.next();
						if (robot == null)
							continue;
						Point2D position = (Point2D) robot.getGlobalPosition();
						if (position == null)
							continue;
						System.out.println(String.format("(%f, %f)", position.x, position.y));
					}
					System.out.println();
				}
			});
		}
		return buttonPrint;
	}

	/**
	 * @return the engine
	 */
	private SycamoreEngine<?> getEngine() {

		return engine;
	}

	/**
	 * @return the uniqueInstance
	 */
	public static CrashedRobotConfigurationPanel getUniqueInstance() {

		if (uniqueInstance == null) {
			uniqueInstance = new CrashedRobotConfigurationPanel();
		}
		return uniqueInstance;
	}

}
